package br.uniriotec.pm;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import io.javalin.Javalin;

public class MainTest {

	/**
	 * Rigorous Test :-)
	 */
	@Test
	public void CHECK_javalin_app_starts_on_port_5555() {
		Javalin app = Javalin.create().start(5555);
		assertEquals(app.port(), 5555);
	}
}