package br.uniriotec.pm.controllers;

import static org.mockito.Mockito.mock;

import org.junit.Test;

import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;

public class UserControllerTest {

	private Context ctx = mock(Context.class);

	@Test(expected = BadRequestResponse.class)
	public void POST_to_create_users_gives_400_for_invalid_context() {
		UserController.create(ctx);
	}

}