package br.uniriotec.pm.models;

public class NewUserRequest {
	// Attributes
	public String name;
	public String email;

	public NewUserRequest() {
	}

	public NewUserRequest(String name, String email) {
		this.name = name;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}
}
